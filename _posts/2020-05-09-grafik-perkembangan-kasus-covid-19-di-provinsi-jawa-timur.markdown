---
title: "Grafik Perkembangan Covid-19 di Provinsi Jawa Timur"
leading: "Karena gue asli Jawa Timur tapi posisi di ibu kota jadinya lumayan intens mengikuti update perkembangan di sana, keluarga besar juga di sana."
date: 2020-05-09 17:33:03 +0700
tags: #Covid19
layout: post
categories: covid19
image_thumb: /asset/post_images/2020-05-09-tangkapan-layar-sebaran-covid-19-di-situs-pemprov-jawa-timur-thumb.jpg
image: /asset/post_images/2020-05-09-tangkapan-layar-sebaran-covid-19-di-situs-pemprov-jawa-timur.jpg
image_credit: Tangkapan layar sebaran kasus Covid-19 di situs pemprov Jawa Timur
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Karena gue asli Jawa Timur tapi posisi di ibu kota, jadinya gue juga lumayan intens mengikuti perkembangan kasus Covid-19 di sana, secara keluarga semua berada di Surabaya.

Jawa Timur sampai hari ini, Sabtu (9/5/2020) tercatat sudah 4 kali pernah menjadi menyumbang data harian terbanyak nasional yaitu pada 12 April, 30 April, 3 Mei dan hari ini 9 Mei 2020 menjadi yang tertinggi dengan 135 kasus. Dengan kondisi seperti ini sepertinya perlu diperhatikan lagi tingkat kewaspadaan masyakarat dan tentu saja pemerintah daerahnya. Padahal Jawa Timur sempat loh dalam beberapa saat pernah menjadi provinsi dengan angka prosentasi recovery index paling bagus.

Oh iya data perkembangan yang gue punya berikut ini sumbernya bukan dari rilis pemprov Jawa Timur, tetapi dari rilis pemerintah pusat. Buat disclaimer aja, karena kalau dicermati, datanya kadang ada selisihnya, entah kenapa :)

<div class="covid__box">
	<h3>Data Kumulatif <span id="toggleScaleTitle"></span></h3>
	<button id="toggleScale">Ubah Linear/Logarithmic</button>
	<div class="covid__canvas -chart1">
		<canvas id="chart1" width="820" height="350"></canvas>
	</div>
	<h3>Data Harian</h3>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="350"></canvas>
	</div>
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="350"></canvas>
	</div>
</div>

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script>
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
var w = document.documentElement.clientWidth;
var type = 'linear';
Chart.defaults.global.layout.padding.right = 20;

var chart1 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Total Kasus',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Dalam Perawatan',
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Perawatan offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Sembuh offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-MeninggalDunia offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Meninggal',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
            label: 'Sembuh',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
            label: 'Positif',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Positif',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 17 %} {{item.Jatim}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Linear)' : '(Logarithmic)';
	chart1.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart1.update();
});

</script>