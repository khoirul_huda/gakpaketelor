---
title: "Grafik Perkembangan Covid-19 di Provinsi DKI Jakarta"
leading: "Memasuki bulan Mei, di ibu kota apakah trennya sudah menurun?"
date: 2020-05-10 09:00:00 +0700
tags: #Covid19
layout: post
categories: covid19
image_thumb: /asset/post_images/2020-05-01-jakarta-menjadi-daerah-dengan-kasus-covid-19-terbanyak-thumb.jpg
image: /asset/post_images/2020-05-01-jakarta-menjadi-daerah-dengan-kasus-covid-19-terbanyak.jpg
image_credit: Jakarta menjadi daerah dengan kasus Covid-19 terbanyak di Indonesia
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Provinsi DKI Jakarta menjadi daerah dengan penyumbang data terbanyak di Indonesia. Memasuki bulan ketiga sejak penemuan kasus pertamanya pada bulan Maret silam dan sudah dibelakukannya PSBB sejak bulan April pula, bagaimana kondisi sekarang? Apakah bisa dikatakan trennya menurun?

Sebagai pengingat sebenarnya datanya nggak bisa langsung dinilai bahwa itu membaik atau memburuk, menurut pakar epidemiologi perlu ada data <a href="https://theconversation.com/indonesia-belum-punya-kurva-epidemi-covid-19-kita-harus-hati-hati-membaca-klaim-pemerintah-kasus-baru-melambat-137497" target="_blank">kurva epidemi Covid-19 yang sesuai dengan standar ilmu epidemiologi</a>, karena ada banyak variabel yang harus disertakan.

Berikut data perkembangan kasus Covid-19 di Provinsi DKI Jakarta.

<div class="covid__box">
	<h3>Data Kumulatif <span id="toggleScaleTitle"></span></h3>
	<button id="toggleScale">Ubah Linear/Logarithmic</button>
	<div class="covid__canvas -chart1">
		<canvas id="chart1" width="820" height="350"></canvas>
	</div>
	<h3>Data Harian</h3>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="350"></canvas>
	</div>
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="350"></canvas>
	</div>
	<!-- <h3>Prosentase Data Kumulatif</h3> -->
	<div class="covid__canvas -chart4">
		<!-- <canvas id="chart4" width="820" height="350"></canvas> -->
	</div>
</div>

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script>
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
// var ctx4 = document.getElementById('chart4').getContext('2d');
var w = document.documentElement.clientWidth;
var type = 'linear';
Chart.defaults.global.layout.padding.right = 20;

var chart1 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
            label: 'Total Kasus',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-TotalKasus offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Dalam Perawatan',
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-Perawatan offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-Sembuh offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-MeninggalDunia offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Meninggal',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
            label: 'Sembuh',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
            label: 'Positif',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Positif',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 17 %} {{item.Jakarta}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Linear)' : '(Logarithmic)';
	chart1.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart1.update();
});


</script>