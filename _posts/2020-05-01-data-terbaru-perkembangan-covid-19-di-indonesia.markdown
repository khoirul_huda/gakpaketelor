---
title: "Data Terbaru Perkembangan Covid-19 di Indonesia"
leading: "Setiap hari Pemerintah memperbarui data kasus Covid-19 di Indonesia. Pengumuman disampaikan oleh juru bicara pemerintah untuk penanganan Covid-19, Ahmad Yurianto."
date: 2020-05-01 22:33:03 +0700
tags: #Covid19
layout: post
categories: covid19
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Dunia sedang ramai Covid-19, dan akhirnya ditemukan kasus pertamanya di Indonesia pada 2 Maret 2020. Hingga hari ini Jum'at (1/5/2020), total laporan kasus terkonfirmasi sudah mencapai lebih dari 10.000 kasus.

Berikut adalah data nasional perkembangan kasus Covid-19 di Indonesia. Data akan diupdate berkala, segera setelah pemerintah memberikan rilis resmi.

<div class="covid__latest col-flex">
	<div class="covid__col -total">
		<span>Total Kasus Terkonfirmasi</span>
		{{site.data.Covid19-Nasional.last.Jumlah_kasus | intcomma: '.'}}
		<i>+{{site.data.Covid19-Nasional.last.Kasus_baru}} Kasus</i>
	</div>
	<div class="covid__col -health">
		<span>Total Sembuh</span>
		{{site.data.Covid19-Nasional.last.Sembuh_kumulatif | intcomma: '.'}}
		<i>+{{site.data.Covid19-Nasional.last.Sembuh_baru}} Kasus</i>
	</div>
	<div class="covid__col -gone">
		<span>Total Meninggal Dunia</span>
		{{site.data.Covid19-Nasional.last.Meninggal_kumulatif | intcomma: '.'}}
		<i>+{{site.data.Covid19-Nasional.last.Meninggal_baru}} Kasus</i>
	</div>
	<div class="covid__col -date">
		<span>Terakhir diperbaharui: {{ "now" | date: "%d/%m/%Y" }}</span>
	</div>
</div>
<div class="covid__box">
	<h3>Data Kumulatif</h3>
	<div class="covid__canvas -chart1">
		<canvas id="chart1" width="820" height="350"></canvas>
	</div>
	<h3>Data Harian <span id="toggleScaleTitle"></span></h3>
	<button id="toggleScale">Ubah Linear/Logarithmic</button>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="350"></canvas>
	</div>
	<h3>Prosentase Data Kumulatif</h3>
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="350"></canvas>
	</div>
	<h3>Data Jumlah Tes Harian</h3>
	<div class="covid__canvas -chart4">
		<canvas id="chart4" width="820" height="350"></canvas>
	</div>
</div>

<strong>Update:</strong> Pada 11 Juni 2020 sesuai <a href="https://twitter.com/BNPB_Indonesia/status/1271003923407486977?s=20" target="_blank">rilis resmi</a> harian yang beredar, Pemerintah meniadakan laporan data <strong>jumlah orang yang dites</strong>, sehingga hanya ada jumlah spesimen yang dites.

<strong>Update:</strong> Setelah sempat ramai di sosial media, karena netizen mempertanyakan 'hilang'-nya data jumlah orang yang dites, akhirnya kurang dari 24 jam Pemerintah lewat Kemenkes meng-update data jumlah orang yang dites. Mungkin masih perlu verifikasi lanjutan kenapa data-nya sempat menghilang.

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
<script>
Chart.plugins.unregister(ChartDataLabels);
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
var ctx4 = document.getElementById('chart4').getContext('2d');
var w = document.documentElement.clientWidth;
var p = (w>768?'bar':'horizontalBar');
var type = 'linear';
Chart.defaults.global.layout.padding.right = 20;

var chart1 = new Chart(ctx, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Nasional %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2a2a2a',
            borderColor: '#2a2a2a',
            label: 'Total Positif',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Jumlah_kasus}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Sembuh_kumulatif}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Perawatan',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Dalam_Perawatan}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Meninggal_kumulatif}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
        layout: {
            padding: {
                right: 55,
			}
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }
		}
	}
});
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Nasional %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Meninggal',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Meninggal_baru}}, {% endfor %}
			]
        },{
            label: 'Sembuh',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Sembuh_baru}}, {% endfor %}
			]
        },{
            label: 'Positif',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-Nasional %} {{item.Kasus_baru}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Nasional offset:9 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Dalam Perawatan',
			fill: 'start',
			borderWidth: 2,
			pointRadius: 0,
            backgroundColor: '#39a6dd63',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Nasional offset:9 %} {{item.Dalam_Perawatan | times: 100.0 | divided_by: item.Jumlah_kasus | round: 2}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'start'
			}
        },{
            label: 'Sembuh',
			fill: '-1',
			borderWidth: 2,
			pointRadius: 0,
            backgroundColor: '#44c64463',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Nasional offset:9 %} {{item.Sembuh_kumulatif | times: 100.0 | divided_by: item.Jumlah_kasus | round: 2}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'start'
			}
        },{
            label: 'Meninggal',
			fill: '-1',
			borderWidth: 2,
			pointRadius: 0,
            backgroundColor: '#f24e4e63',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Nasional offset:9 %} {{item.Meninggal_kumulatif | times: 100.0 | divided_by: item.Jumlah_kasus | round: 2}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'start'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
        layout: {
            padding: {
                right: 55,
			}
		},
        scales: {
            yAxes: [{
				stacked: true,
                ticks: {
					min: 0,
					max: 100,
					stepSize: 20,
                    callback: function(value, index, values) {
                        return value + '%';
                    }
                }
            }]
        },
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		},
        plugins: {
            filler: {
                propagate: true
            },
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#000',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return value + '%';
                }
            }
        }
	}
});

var chart4 = new Chart(ctx4, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Nasional offset: 46 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			type: 'line',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            label: 'Terkonfirmasi positif harian',
            data: [
				{% for item in site.data.Covid19-Nasional offset: 46 %} {{item.Kasus_baru}}, {% endfor %}
			]
        },{
			type: 'line',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            label: 'Orang yang dites harian',
            data: [
				{% for item in site.data.Covid19-Nasional offset: 46 %} {{item.Test_harian_orang}}, {% endfor %}
			]
        },{
            label: 'Spesimen yang dites harian',
            backgroundColor: '#d5d5d5',
            borderColor: '#d5d5d5',
            data: [
				{% for item in site.data.Covid19-Nasional offset: 46 %} {{item.Test_harian}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		tooltips: {
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Linear)' : '(Logarithmic)';
	chart2.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart2.update();
});

</script>