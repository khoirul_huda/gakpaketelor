---
title: "Grafik Perkembangan Covid-19 di Provinsi Jawa Barat"
leading: "Sekilas menurut gue sebagai orang awam, Jawa Barat paling oke nih, tapi kalau menurut data gimana ya?"
date: 2020-05-10 16:30:00 +0700
tags: #Covid19
layout: post
categories: covid19
image_thumb: /asset/post_images/2020-05-10-tampilan-situs-pikobar-jawa-barat-thumb.jpg
image: /asset/post_images/2020-05-10-tampilan-situs-pikobar-jawa-barat.jpg
image_credit: Tampilan situs Pikobar sebagaui media rilis resmi informasi Covid-19 di provinsi Jawa Barat
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Sekilas menurut gue sebagai orang awam, Jawa Barat paling oke nih dalam menanganan Covid-19, tapi kalau menurut data gimana ya? 

Again ya, sebagai pengingat sebenarnya datanya nggak bisa langsung dinilai bahwa itu membaik atau memburuk, menurut pakar epidemiologi perlu ada data <a href="https://theconversation.com/indonesia-belum-punya-kurva-epidemi-covid-19-kita-harus-hati-hati-membaca-klaim-pemerintah-kasus-baru-melambat-137497" target="_blank">kurva epidemi Covid-19 yang sesuai dengan standar ilmu epidemiologi</a>, karena ada banyak variabel yang harus disertakan.

Tetapi untuk dilihat secara kasat mata, berikut data perkembangan kasus Covid-19 di provinsi Jawa Barat. Sumber data masih yang dari rilis pemerintah pusat ya bukan pemerintah daerah. Oh iya, katanya keterbukaan data Covid-19 oleh pemerintah provinsi Jawa Barat adalah termasuk yang lengkap ya? Penasaran, nanti gue cari-cari ah di Pikobar.

<div class="covid__box">
	<h3>Data Kumulatif <span id="toggleScaleTitle"></span></h3>
	<button id="toggleScale">Ubah Linear/Logarithmic</button>
	<div class="covid__canvas -chart1">
		<canvas id="chart1" width="820" height="350"></canvas>
	</div>
	<h3>Data Harian</h3>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="350"></canvas>
	</div>
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="350"></canvas>
	</div>
</div>

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script>
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
var w = document.documentElement.clientWidth;
var type = 'linear';
Chart.defaults.global.layout.padding.right = 20;

var chart1 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset: 17 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
            label: 'Total Kasus',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-TotalKasus offset: 17 %} {{item.Jabar}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Dalam Perawatan',
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-Perawatan offset: 17 %} {{item.Jabar}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-Sembuh offset: 17 %} {{item.Jabar}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-MeninggalDunia offset: 17 %} {{item.Jabar}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		//spanGaps: false,
		//elements: {
		//	line: {
		//		tension: 0.000001
		//	}
		//},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 131 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Meninggal',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        },{
            label: 'Sembuh',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        },{
            label: 'Positif',
			fill: false,
			borderWidth: 2,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-KasusHarian offset: 131 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
			fill: false,
            label: 'Meninggal',
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-MeninggalDuniaHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Sembuh',
            backgroundColor: '#44C644',
            borderColor: '#44C644',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-SembuhHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        },{
			fill: false,
            label: 'Positif',
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
			borderWidth: 1.5,
			pointRadius: .5,
            data: [
				{% for item in site.data.Covid19-KasusHarian offset: 131 %} {{item.Jabar}}, {% endfor %}
			]
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: false
		},
		maintainAspectRatio: false,
        responsive: false,
		onResize: function(){
			
		}
	}
});

var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Logarithmic)' : '(Linear)';
	chart1.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart1.update();
});


</script>