---
title: "Grafik Perkembangan Angka Kasus Aktif Covid-19 di 34 Provinsi"
leading: "Melihat dinamika perkembangan jumlah angka pasien dalam perawatan (kasus aktif) tiap provinsi di Indonesia."
date: 2020-06-12 22:01:03 +0700
tags: #Covid19
layout: post
image_thumb: /asset/post_images/2020-06-13-grafik-perkembangan-angka-kasus-aktif-covid-19-di-34-provinsi-thumb.jpg
image: /asset/post_images/2020-06-13-grafik-perkembangan-angka-kasus-aktif-covid-19-di-34-provinsi.jpg
image_credit: Protective masks, normally used for surgery, are now in use to fight the Corona Virus SARS-nCov-19. (Photo by Mika Baumeister on Unsplash)
categories: covid19
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Bulan Juni ini pemerintah sudah mulai melonggarkan kegiatan di luar rumah, beberapa daerah sudah mengakhiri masa PSBB-nya. Era "New Normal" pun sudah dimulai. 

Kali ini kita akan melihat perkembangan kasus aktif atau pasien yang sedang dalam perawatan Covid-19. Kategori dalam perawatan di sini adalah semua yang masih terkonfirmasi positif Covid-19, entah itu pasien yang sedang dalam perawatan intensif di rumah sakit ataupun karantina mandiri di rumah masing-masing. 

Grafik berikut ini hanya sebagai gambaran awam yang berguna untuk melihat jumlah kasus aktif Covid-19 itu masih berapa banyak sih? Karena jika kita hanya fokus melihat grafik angka kumulatif positif Covid-19, maka angkanya tidak akan pernah turun.

Sebenarnya grafik perkembangan angka pasien dalam perawatan bukan serta merta menjadi petunjuk sahih bahwa tren sebaran Covid-19 sudah menurun apa belum. Ada banyak variabel yang harus dilihat jika tujuannya untuk itu, seperti angka kesembuhan seberapa banyak tes yang dilakukan, berapa banyak populasi, dan lain-lain.

Angka kasus aktif di sini didapat dari jumlah kasus konfirmasi positif suatu daerah dikurangi angka kesembuhan dan angka kematian.

Sekali lagi bahwa angka-angka berikut bersumber dari rilis nasional, bukan dari data yang dirilis tiap daerah.

<div class="covid__latest col-flex">
	<div class="covid__col -date">
		<span>Terakhir diperbaharui: {{ "now" | date: "%d/%m/%Y" }}</span>
	</div>
</div>
<div class="covid__box">
	<h3>Data Kumulatif Kasus Aktif Nasional</h3>
	<div class="covid__canvas -chart1">
		<canvas id="chart1" width="820" height="400"></canvas>
	</div>
	<h3>Data Kumulatif Kasus Aktif Provinsi Tertinggi <span id="toggleScaleTitle"></span></h3>
	<!-- <button id="toggleScale">Ubah Linear/Logarithmic</button> -->
	<div class="covid__canvas -chart2">
		<canvas id="chart11" width="820" height="400"></canvas>
	</div>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="420"></canvas>
	</div>
	<!-- <h3>10 Provinsi Tertinggi Berikutnya <span id="toggleScaleTitle"></span></h3> -->
	<!-- <button id="toggleScale">Ubah Linear/Logarithmic</button> -->
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="420"></canvas>
	</div>
	<div class="covid__canvas -chart4">
		<canvas id="chart4" width="820" height="420"></canvas>
	</div>
</div>

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
<script>
Chart.plugins.unregister(ChartDataLabels);
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx11 = document.getElementById('chart11').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
var ctx4 = document.getElementById('chart4').getContext('2d');
var w = document.documentElement.clientWidth;
var p = (w>768?'bar':'horizontalBar');
Chart.defaults.global.layout.padding.right = 20;
var type = 'linear';
var chart = new Chart(ctx, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',
    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Nasional offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Kasus Aktif / Dalam Perawatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#b758ff',
            borderColor: '#b758ff',
            data: [
				{% for item in site.data.Covid19-Nasional offset:31 %} {{item.Dalam_Perawatan}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		spanGaps: false,
		elements: {
			line: {
				tension: 0.000001
			}
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 50,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }			
		}
	}
});

var chart11 = new Chart(ctx11, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Perawatan offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Jawa Tengah',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Jateng}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jakarta',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Jakarta}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jawa Barat',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Jabar}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jawa Timur',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Jatim}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Banten',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#c923ff',
            borderColor: '#c923ff',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Banten}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 50,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }			
		}
	}
});

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Perawatan offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Papua',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Papua}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sulawesi Selatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sulsel}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Kalimantan Timur',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#c923ff',
            borderColor: '#c923ff',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Kaltim}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'DIY',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.DIY}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sumatera Barat',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2e3335',
            borderColor: '#2e3335',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sumbar}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 50,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }			
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Perawatan offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Sumatera Utara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sumut}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sulawesi Utara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sulut}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Kalimantan Utara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#c923ff',
            borderColor: '#c923ff',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Kaltara}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Kalimantan Tengah',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2e3335',
            borderColor: '#2e3335',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Kalteng}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Lampung',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Lampung}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 50,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }			
		}
	}
});

var chart4 = new Chart(ctx4, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-Perawatan offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Sumatera Selatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sumsel}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Maluku',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Maluku}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Aceh',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Aceh}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sulawesi Tenggara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Sultra}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Riau',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#c923ff',
            borderColor: '#c923ff',
            data: [
				{% for item in site.data.Covid19-Perawatan offset:31 %} {{item.Riau}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 50,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }			
		}
	}
});

/*var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Linear)' : '(Logarithmic)';
	chart2.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart2.update();
});*/

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

</script>