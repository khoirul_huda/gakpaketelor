---
title: "Data Sebaran Kasus Covid-19 di 34 Provinsi"
leading: "Kali ini lebih kepada sebaran data Covid-19 pada tiap provinsi di Indonesia."
date: 2020-05-03 16:26:03 +0700
tags: #Covid19
layout: post
categories: covid19
image_thumb: /asset/post_images/2020-05-03-tangkapan-layar-kompascom-covid19-thumb.jpg
image: /asset/post_images/2020-05-03-tangkapan-layar-kompascom-covid19.jpg
image_credit: Tangkapan layar data sebaran Covid-19 di Kompas.com
comments: true
#js: ['/asset/js/chartist.min.js','/asset/js/covid-nasional.js']
---

Dunia sedang ramai Covid-19, dan akhirnya ditemukan kasus pertamanya di Indonesia pada 2 Maret 2020. Hingga hari ini Minggu (3/5/2020), total laporan kasus terkonfirmasi sudah mencapai lebih dari 10.000 kasus.

Pada postingan kali ini gue coba bikin data perkembangan Covid-19 di tiap provinsi. Nantinya (kalau bisa) bakal coba bikin beragam model grafik, untuk mencari tahu apakah ada informasi menarik yang bisa dilihat.

Sementara masih malihat-lihat data yang tersedia dan kemungkinan bisa ditampilkan dengan model seperti apa, berikut adalah grafik secara umum sebaran data Covid-19 tiap provinsi di Indonesia.

Seperti halnya data nasional, data provinsi juga akan diupdate berkala, segera setelah pemerintah memberikan rilis resmi.

<div class="covid__latest col-flex">
	<div class="covid__col -date">
		<span>Terakhir diperbaharui: {{ "now" | date: "%d/%m/%Y" }}</span>
	</div>
</div>
<div class="covid__box">
	<h3>Data Kumulatif</h3>
	<div class="covid__canvas -chart-prov-total">
		<canvas id="chart1"></canvas>
	</div>
	<h3>Data Kumulatif 5 Provinsi Tertinggi <span id="toggleScaleTitle"></span></h3>
	<button id="toggleScale">Ubah Linear/Logarithmic</button>
	<div class="covid__canvas -chart2">
		<canvas id="chart2" width="820" height="420"></canvas>
	</div>
	<h3>Data Kumulatif Provinsi Tertinggi Berikutnya <span id="toggleScaleTitle"></span></h3>
	<!-- <button id="toggleScale">Ubah Linear/Logarithmic</button> -->
	<div class="covid__canvas -chart3">
		<canvas id="chart3" width="820" height="420"></canvas>
	</div>
	<div class="covid__canvas -chart4">
		<canvas id="chart4" width="820" height="420"></canvas>
	</div>
</div>

Catatan: <b>"Lainnya"</b> yang dimaksud pada grafik adalah data yang masih dalam proses verifikasi di lapangan.

Sumber data: <a href="https://kawalcovid19.id/" target="_blank">KawalCOVID19.id</a>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
<script>
Chart.plugins.unregister(ChartDataLabels);
const covNas = document.getElementById('covid-nasional');
var ctx = document.getElementById('chart1').getContext('2d');
var ctx2 = document.getElementById('chart2').getContext('2d');
var ctx3 = document.getElementById('chart3').getContext('2d');
var ctx4 = document.getElementById('chart4').getContext('2d');
var w = document.documentElement.clientWidth;
var p = (w>768?'bar':'horizontalBar');
Chart.defaults.global.layout.padding.right = 20;
var type = 'linear';

var chart = new Chart(ctx, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus.last offset:1 %} '{{item[0]}}', {% endfor %}
		],
        datasets: [{
            label: 'Total Positif',
            backgroundColor: function(context) {
				var index = context.dataIndex;
				var value = context.dataset.data[index];
				console.log(index + ' ' +value)
				return value > 40000 ? '#770041' : value > 10000 ? '#d73e92' : value > 4000 ? '#f18daf' : '#fadbd4'
			},
            borderColor: '#fff',
            data: [
				{% for item in site.data.Covid19-TotalKasus.last offset:1 %} '{{item[1]}}', {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Kumulatif'
		},
		tooltips: {
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: true,
		plugins: {
            datalabels: {
				font: {
					size: 11,
					weight: '500'
				},
                formatter: function(value, context) {
                    return addCommas(value)
                }
			}
		}
	}
});

var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Jakarta',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Jakarta}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jawa Timur',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Jatim}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jawa Tengah',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Jateng}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Jawa Barat',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Jabar}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sulawesi Selatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2e3335',
            borderColor: '#2e3335',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Sulsel}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 55,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }
		}
	}
});

var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Sumatera Barat',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Sumbar}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Kalimantan Timur',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#F24E4E',
            borderColor: '#F24E4E',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Kaltim}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sumatera Utara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2e3335',
            borderColor: '#2e3335',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Sumut}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Bali',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Bali}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Kalimantan Selatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Kalsel}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 55,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }
		}
	}
});

var chart4 = new Chart(ctx4, {
    // The type of chart we want to create
    plugins: [ChartDataLabels],
    type: 'line',

    // The data for our dataset
    data: {
        labels: [
			{% for item in site.data.Covid19-TotalKasus offset:31 %} '{{item.Date | date: "%d %b"}}', {% endfor %}
		],
        datasets: [{
            label: 'Banten',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#44C644',
            borderColor: '#44C644',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Banten}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sumatera Selatan',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#FE9B44',
            borderColor: '#FE9B44',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Sumsel}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Papua',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#3CA5DD',
            borderColor: '#3CA5DD',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Papua}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        },{
            label: 'Sulawesi Utara',
			fill: false,
			borderWidth: 1.5,
			pointRadius: .5,
            backgroundColor: '#2e3335',
            borderColor: '#2e3335',
            data: [
				{% for item in site.data.Covid19-TotalKasus offset:31 %} {{item.Sulut}}, {% endfor %}
			],
			datalabels: {
				align: 'right',
				anchor: 'end'
			}
        }]
    },

    // Configuration options go here
	options: {
		title: {
			display: false,
			text: 'Data Harian'
		},
		scales: {
			xAxes: [{
				display: true,
			}],
			yAxes: [{
				display: true,
				type: type
			}]
		},
		tooltips: {
			position: 'nearest',
			mode: 'index',
			intersect: true
		},
		maintainAspectRatio: false,
        responsive: false,
        layout: {
            padding: {
                right: 55,
			}
		},
		plugins: {
            datalabels: {
				backgroundColor: function(context) {
					return context.dataset.backgroundColor;
				},
				borderRadius: 4,
				color: '#fff',
				font: {
					size: 11,
					weight: '500'
				},
				display: function(context) {
					var mx = context.dataset.data.length
					var i = context.dataIndex
					return i == (mx-1) ? true : false
				},
                formatter: function(value, context) {
				    var index = context.dataIndex;
                    return addCommas(value)
                }
            }
		}
	}
});

var title = document.getElementById('toggleScaleTitle');
document.getElementById('toggleScale').addEventListener('click', function() {
	type = type === 'linear' ? 'logarithmic' : 'linear';
	title.innerHTML = type === 'linear' ? '(Linear)' : '(Logarithmic)';
	chart2.options.scales.yAxes[0] = {
		display: true,
		type: type
	};

	chart2.update();
});

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

</script>