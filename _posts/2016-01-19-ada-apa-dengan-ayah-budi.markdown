---
title:  "Ada Apa dengan (Ayah) Budi?"
date:   2016-01-19 17:33:13 +0700
tags: #30haribercerita
layout: post
categories: 30haribercerita
comments: true
---

Budi bermimpi, ia dan ibunya pergi ke pabrik untuk menjemput ayahnya yang siang itu sengaja izin pulang lebih cepat. Mereka ber- tiga pergi ke pantai untuk sejenak melepas penat. Sudah sekian lama mereka tidak berlibur bersama, sejak Budi bekerja di kota seberang.

“Ayo, Yah! Kita adu lari!”<br>
“Yuk, Ayah hitung sampai tiga.”<br>
“Siap... Tiga!”<br>
“Ah, Ayah curang!!!”<br>

Mereka lantas berlari kencang menuju bibir pantai dengan riang-nya. Ibu Budi duduk di bawah payung besar beralaskan tikar lipat lawas yang biasa mereka pakai saat liburan sejak Budi masih kecil. Sambil sibuk menyiapkan bekal untuk dimakan bersama nantinya, sesekali tampak senyum mengembang dari bibir ibunya ketika Budi dan ayahnya dari kejauhan melambai ke arahnya.

Ketika sedang asyik berenang bersama, tiba-tiba ombak besar tanpa aba-aba menyeret ayah Budi, menelannya mentah-mentah. Kejadiannya sangat cepat, Budi tak tahu pasti apa yang sedang ter- jadi sebenarnya. Yang ia tahu, ayahnya hilang ditelan ombak dengan mudahnya. Sampai-sampai hanya untuk berteriak memanggil nama ayahnya saja ia tak sempat. Ayah Budi lenyap dalam sekejap mata.

Seketika Budi langsung tersadar dari mimpinya. Ia terbangun dengan tubuh bersimbah keringat dan muka pucat kesi. Dengan na- fas yang masih terengah-engah, ia lantas mengambil telepon geng- gam untuk segera menelepon ibunya tapi tak bisa karena kehabisan pulsa.

Di pagi harinya, Budi bergegas pulang. Akibat mimpi buruk semalam, hari itu Budi kalut sejadi-jadinya. Firasat buruk mulai menguasai pikiran Budi. Ia takut sesuatu terjadi pada ayahnya. Ayah Budi memang sudah lama sakit stroke, sedangkan ia tak bisa menjaga ayahnya setiap hari karena harus bekerja di kota seberang. Ia hanya bisa mengunjungi ayahnya seminggu sekali. Dari kontrakan tanpa pikir panjang langsung saja Budi menghampiri pangkalan ojek untuk segera pulang ke rumah.

“Bang, bisa ngebut dikit nggak? Buru-buru nih!”<br>
“Kenapa emangnya, Dek?”<br>
“Genting, deh, bang pokoknya!”<br>

Bendera kuning menyambut kedatangan Budi di rumah. Ia juga langsung disambut pelukan haru oleh ibunya, tentu saja sambil ber- linang air mata, dan suara terbata-bata. Budi mulai lemas, pelupuk matanya terasa berat, bibirnya berge- tar, dan kerongkongan mulai terasa sakit. Dilihatnya langsung wajah pucat ayahnya, senyum kaku itu masih tampak sama seperti yang ia lihat sewaktu berenang bersama di pantai. Seketika deras air matanya semakin menjadi-jadi.

Tin... Tin... Tin!!

Bunyi klakson motor dari belakang seketika membuyarkan la- munan Budi. Dilihatnya lampu lalu lintas sudah berubah menjadi hi- jau. Ia tersadar ternyata yang tadi itu hanya lamunannya saja. Firasat buruk semakin merajalela menguasai pikirannya. Ia sungguh khawa- tir, dalam hatinya ia berdoa, Ya Allah, semoga ayah baik-baik saja.

Sesampainya di rumah, Budi melihat bendera kuning berkibar, persis seperti lamunannya tadi. Pikirannya langsung teringat tentang mimpinya semalam. Tapi ia heran, tak seorangpun menyambutnya, ibunya hanya tertunduk di ruang tengah. Di samping ibunya ada ayahnya terbaring, tapi tak terlihat pucat, hanya seperti ada bekas tangisan. Budi semakin heran, ketika ia melihat ada sesosok yang mirip dengannya terbaring dengan wajah pucat mati. Dan di luar rumah terdengar bincang-bincang tentang kecelakaan tadi pagi yang merenggut nyawa tukang ojek dan penumpangnya.

-tamat-

<strong><em>Tulisan ini adalah salah satu tulisan saya #30hariBercerita yang kebetulan masuk dalam kompilasi buku "Ini Mimpi Budi" bersama penulis #30hariBercerita lainnya. Klik <a href="http://30haribercerita.blogspot.co.id/2017/01/buku-ini-mimpi-budi.html" target="_blank">di sini</a> untuk mengetahui detail bukunya.</em></strong>