---
title: "Ibu Kota Hari Ini"
date: 2017-01-01 17:33:13 +0700
tags: #30haribercerita
layout: post
categories: 30haribercerita
image_thumb: /asset/post_images/2017-01-01-ibu-kota-hari-ini-thumb.jpg
image: /asset/post_images/2017-01-01-ibu-kota-hari-ini.jpg
image_credit: Ketika hujan di Jakarta (dok. pribadi)
comments: true
---

Kulihat awan gelap bergerak sesuka hati, seperti tak patuh pada arah angin. Meski senja sudah di ujung ufuk, kawanan awan itu terlalu jelas untuk dihiraukan dari samarnya cahaya senja. Aku mulai berdoa semoga mendung kali ini bersahabat dengan kami.

Ketika hujan lebat mulai turun, aku semakin berhati-hati mengendalikan kendaraanku menyusuri jalanan ibu kota yang tak pernah sepi. Aku pelankan kendaraanku, mendekat beberapa meter jaraknya dari gedung perkantoran megah itu. Tampak beberapa orang mulai sebal, sungguh jelas terlihat dari raut wajah mereka seolah ingin mengumpat kasar. 

Sedikit pun aku tak pernah sebal dengan hujan, karena hujan adalah ganjaran. Hujan adalah jawaban setelah awan gelap muncul memberikan pertanda. Mirip dengan sebuah janji yang telah ditepati bukan? 

Sebenarnya, kenapa sih hujan harus dilawan? Kenapa musti ditantang? Kenapa pula nekat diterjang? Kenapa harus sedia payung sebelum hujan? Kenapa tak tunggu saja di situ. Tak bisakah kamu sebentar saja menikmatinya? Toh setelah hujan reda alam akan memperlihatkan keindahannya, dengan munculnya pelangi. Ah iya, tapi langit yang sudah berganti gelap tak mungkin muncul pelangi. 

Tapi, tidak denganku hari ini. Aku tak harus menunggu sampai hujan berhenti. Aku sudah punya pelangi sendiri. Pelangiku sudah menanti sedari tadi. Sosoknya perlahan muncul dari balik kerumunan orang dengan penampakan kulit kuning langsat, rambut lurus sebahu, wajah berseri dengan sepasang bola mata indah berwarna cokelat dilengkapi dengan kacamata bulat ala Danilla. Ah, sempurna. 

Langkah kecilnya yang imut berkejaran dengan hujan. Cepat sekali dia menuju ke arahku, dan aku sigap membuka pintu kendaraanku untuknya. <br>
"Lama banget sik! Capek tauk nunggu dari tadi!" Wajah cantiknya makin mempesona ketika dia marah, dan pandanganku masih tak bisa berpaling darinya. <br>
"Heh, mas! Kenapa lo? Jangan aneh-aneh deh. Lo mau ntar gue kasih rate satu bintang?!" <br>
"Oh, enggak mbak, ampun." <br>
"Duh, hari ini dapet penumpang galak nih keknya," gumamku.
