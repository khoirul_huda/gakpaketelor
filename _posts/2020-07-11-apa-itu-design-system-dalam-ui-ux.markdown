---
pretag: "#UI/UX"
title: "Apa Itu Design System?"
leading: "Dalam beberapa tahun terakhir lumayan populer di kalangan para designer. Sekilas membahas tentang design system dalam UI/UX."
date: 2020-07-11 09:21:19 +0700
tags: #design
layout: post
image_thumb: /asset/post_images/2020-07-11-what-is-design-system-in-ui-ux-thumb.jpg
image: /asset/post_images/2020-07-11-what-is-design-system-in-ui-ux.jpg
image_credit: Buku berjudul Design Systems karya Alla Kholmatova yang membahas tentang panduan praktis membangun Design System untuk produk digital. 
categories: design
comments: true
---

Halo semua. Kali ini akan membahas sedikit tentang *design system* dalam dunia UI/UX. Kita juga akan sekilas mengintip *brand* apa saja di Indonesia yang sudah mengaplikasikannya dalam produk mereka.

Oh iya, dalam artikel ini masih belum membahas secara detail ya, hanya sedikit saja. Jadi, nanti di lain kesempatan ~~kalau sudah agak jago~~ akan dibahas lebih detail lagi. HAHA *lesgo!*

<p style="text-align: center">. &nbsp; . &nbsp; .</p>

Dalam beberapa tahun terakhir, istilah *design system* menjadi lumayan populer di kalangan para *designer*. Apalagi bagi mereka yang bekerja di dalam tim dengan anggota yang banyak, *design system* kini menjadi salah satu pengetahuan yang penting. 

*Design system* sesungguhnya bukan pengetahuan spesifik hanya untuk *designer* saja, semua pihak yang terkait dengan pengembangan produk, termasuk *developer* semestinya paham juga.

## Definisi Design System

Sebenarnya tidak ada definisi baku dari *design system*, orang-orang cenderung mempunyai penjelasan yang berbeda-beda dalam mendefinisikannya.

Menurut Alla Kholmatova dalam buku berjudul *"Design Systems"* terbitan Smashing Magazine, *design system* adalah kumpulan komponen yang saling terhubung dan <ins>**terintegrasi**</ins> dengan suatu sistem yang sudah disepakati untuk kemudian bisa digunakan secara bersama-sama. 

Sedangkan Sam Anderson—*salah satu anggota tim Adobe XD*—dalam [videonya](https://www.youtube.com/watch?v=PWfEHsqC1Ng){:target="_blank"} menjelaskan bahwa *design system* adalah kumpulan komponen dari suatu desain yang <ins>**terdokumentasi**</ins> dengan baik sehingga bisa digunakan kembali pada waktu yang akan datang.

Nah, ada dua kata kunci yang perlu digarisbawahi dari penjelasan di atas karena sangat krusial, yaitu sebuah *design system* yang bagus haruslah <ins>**terintegrasi**</ins> dan <ins>**terdokumentasi**</ins> dengan baik.

Kalau gabungan versi gampangnya sih, *design system* adalah sebuah sistem yang dibuat atas kesepakatan bersama, yang menjelaskan **cara kita berkerja, cara kita berkolaborasi** dalam rangka mengembangkan sebuah produk, dan semua tim harus patuh menjalankan sistem tersebut, dari mulai *product owner*, *designer*, sampai *developer*.

## Kenapa kita butuh Design System?

Dalam proses pengembangan atau pembuatan sebuah produk, tidak mutlak harus memakai *design system*, tergantung kondisi dan kebutuhan. Secara umum kondisi yang biasanya akan membuat kita berpikir sudah saatnya membutuhkan *design system* adalah sebagai berikut.

* ### ***Multiple designers and stakeholders***  
Karri Saarinen—*Principal Designer di Airbnb*-dalam [artikelnya menceritakan](https://airbnb.design/building-a-visual-language/){:target="_blank"}, ketika Airbnb tumbuh dari tahun ke tahun, semakin bertambah pula anggota di balik layar. Sekarang ini, dalam tim desain saja sudah lebih dari selusin *designer*. Setelah itu akhirnya terlihat jelas pekerjaan menjadi semakin kompleks dan karena itulah mereka butuh cara bekerja yang sistematis.

* ### ***Multi-platform***  
Ketika kita mempunyai produk yang *running* di berbagai jenis platform, produk kita harus mempunyai fitur yang berfungsi sama baiknya. Sebagai contoh misalnya produk kita adalah pemesanan tiket pesawat. Tentunya semua fitur harus bisa berjalan sempurna baik di *desktop*, *mobile web*, dan *app*. Ketika ada sebuah pengembangan fitur pada versi *app*, maka kita harus melakukan pekerjaaan serupa untuk diimplementasikan di platform lainnya. Nah, akan lebih mudah apabila kita mempunyai suatu sistem terintegrasi yang bisa menjaga fitur dan tampilan agar tetap selaras.

<div class="main__img">
	<img src="/asset/post_images/2020-07-11-multi-platform-multiple-designer.jpg" alt="Ilustrasi sebuah tim yang mengerjakan produk dengan multi-platform.">
	<div class="main__img__credit">
		Ilustrasi sebuah tim yang mengerjakan produk dengan multi-platform. (Grafis dibuat dengan material dari Freepik dan Flaticon)
	</div>
</div>

## Terus, apa manfaat Design System?

* ### ***Consistency in design***  
Sedikit berbeda dengan disiplin ilmu desain lainnya, dalam mendesain sebuah produk digital sangat memungkinkan untuk menemukan berbagai macam cara dalam menyelesaikan sebuah permasalahan, tetapi hal itu kadang bisa menjadi sangat liar. Jika tidak ada batasan yang dibuat maka tampilan produk kita akan berbeda-beda tergantung selera *designer*, dan hal itu bisa berisiko *user experience* produk kita menjadi tidak nyaman untuk pengguna. Nah, dengan adanya batasan-batasan pada *design system*, diharapkan dapat menjaga kualitas *user experience* produk kita. 

<div class="main__img">
	<img src="/asset/post_images/2020-07-11-airbnb-design-system-language.png" alt="Konsistensi tampilan navigasi aplikasi Airbnb, dibuat sama baik di iOS dan Android agar user experience terasa nyaman.">
	<div class="main__img__credit">
		Konsistensi tampilan navigasi aplikasi Airbnb, dibuat sama baik di iOS dan Android agar user experience terasa nyaman. (Gambar diambil dari <a href="https://design.google/library/airbnb-communicating-clarity-and-charm/" target="_blank">Google Design</a>)
	</div>
</div>

* ### ***Efficiency***  
Salah satu prinsip dasar yang dipakai di *design system* adalah komponen berbasis *reusable*. Ketika mendesain sebuah komponen kita juga harus berpikir bahwa komponen tersebut akan bisa digunakan di *layout* lainnya. Ketika sewaktu-waktu ada fitur baru yang menggunakan komponen tadi, maka kita tidak perlu membuat komponen baru cukup mengambil dari *library*. Dari contoh simpel ini saja sudah terlihat bisa menghemat banyak waktu dalam proses pengembangan produk.

* ### ***Teamwork and collaboration***  
Salah satu poin penting adanya *design system* adalah mengubah *mindset* cara kita bekerja, bukan lagi individual tetapi kolaboratif. *Design system* bukan semata-mata tentang hasil akhir yaitu produk yang kita luncurkan kepada pengguna. Membangun *design system* juga adalah membuat sebuah *legacy* untuk orang-orang supaya di lain kesempatan bisa menggunakan kembali sistem yang kita rancang.

## Brand di Indonesia yang menggunakan Design System

Sebenarnya mungkin sudah banyak *brand* yang menggunakan *design system* di internal tim mereka, hanya saja masih sedikit *brand* yang memperlihatkan *design system* versi mereka kepada khalayak umum. 

Beberapa *brand* yang sudah menyajikannya dalam bentuk web dokumentasi adalah [Asphalt](https://asphalt.gojek.io/){:target="_blank"} dari Gojek, kemudian ada [Tedis](https://tedis.telkom.design/){:target="_blank"} buatan Telkom, dan juga ada [Bukalapak Design](https://brand.bukalapak.design/){:target="_blank"}.

Oh iya, yang saya tahu masih sebatas itu, apabila ada tambahan *brand* lokal yang menurut kalian keren *design system*-nya, akan sangat menyenangkan jika kalian menulis di kolom komentar, kita diskusi bareng!

<p style="text-align: center">. &nbsp; . &nbsp; .</p>

*Okay* segini dulu pembahasan *design system* kali ini. Berikutnya mungkin akan membahas lebih detail lagi tentang *design system*. Jika ada kesalahan fatal dalam artikel ini, sangat tebuka untuk menerima masukan. *Thanks!*

## Referensi

* [Design Systems: A pratical guide to creating design languages for digital products.](https://www.smashingmagazine.com/design-systems-book/){:target="_blank"}
* [Airbnb Design: Building a Visual Language](https://airbnb.design/building-a-visual-language/){:target="_blank"}
* [Designing Adobe XD: Design Systems](https://www.youtube.com/watch?v=PWfEHsqC1Ng){:target="_blank"}

<p style="text-align: center">. &nbsp; . &nbsp; .</p>
