function bukaPeta() {
	var w = document.documentElement.clientWidth;
	// if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	if(w<768) {
		var defaultZoom = 4;
	}
	else {
		var defaultZoom = 5;
	}
	var defaultCenter = new google.maps.LatLng(-2.1944434, 117.9563994);
	//var defaultZoom = 4;
	var mapStyleOptions = [{
	    "featureType": "poi",
	    "elementType": "labels.icon",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, {
	    "featureType": "transit.station",
	    "elementType": "labels.icon",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, 
	  {elementType: 'geometry', stylers: [{color: '#2bb999'}]},
{
            "featureType": "all",
            "elementType": "labels",
            "stylers": [{
                "visibility": "off"
            }, {
                "saturation": "-100"
            }]
        },
      {elementType: 'labels.text.stroke', stylers: [{color: '#fff'}]},
      {elementType: 'labels.text.fill', stylers: [{color: '#000'}]},					
	  {
		  featureType: 'water',
		  elementType: 'geometry',
		  stylers: [{color: '#dffff8'}]
	  }, {
	    "featureType": "road.local",
	    "elementType": "labels.text",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, {
	    "featureType": "landscape",
	    "elementType": "labels.icon",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, {
	    "featureType": "administrative.locality",
	    "elementType": "labels.text.fill",
	    "stylers": [{
	      "color": "#2a2a2a"
	    }, {
	      "weight": 1
	    }]
	  }, {
	    "featureType": "administrative.neighborhood",
	    "elementType": "geometry",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, {
	    "featureType": "road.highway",
	    "elementType": "labels.icon",
	    "stylers": [{
	      "visibility": "off"
	    }]
	  }, {
	    "featureType": "administrative.province",
	    "elementType": "geometry.stroke",
	    "stylers": [{
	      "weight": 2
	    }, {
	      "color": "#2a2a2a"
	    }]
	  }
	];
	var styledMap = new google.maps.StyledMapType(mapStyleOptions, {name: "Styled Map"});
	
	var locations = [];
	var flags = [];
	var calons = [];
	var dtMp = dft.aaData;
	var tampil, fill, label, calon, wcalon, scale, jalur, noUrut;
	for (var i=0; i < dtMp.length; i++) {
		tampil = dtMp[i].idWil;
		
		// calon yg lolos
		if(dtMp[i].status>1) {

			if (dtMp[i].jalur == "individu") { jalur = "(jalur perseorangan)" }
			else { jalur = "(dicalonkan oleh " + dtMp[i].jalur +")"}

			// nyari calon2, dikumpulkan jadi satu Wilayah
			if(flags[dtMp[i].idWil]) {
				// calon2 di jadiin satu var dulu
				for(var j=0; j<locations.length; j++) {
					if(locations[j].idWil == dtMp[i].idWil) {
						//locations[j].noUrut = locations[j].noUrut + ";" + dtMp[i].noUrut;
						//locations[j].calon = locations[j].calon + ";" + dtMp[i].Kd;
						//locations[j].wcalon = locations[j].wcalon + ";" + dtMp[i].Wkd;
						//locations[j].jalur = locations[j].jalur + ";" + jalur;
						locations[j].calons.push({noUrut:dtMp[i].noUrut,Kd:dtMp[i].Kd,Wkd:dtMp[i].Wkd,jalur:jalur});
					}
				}
				continue;
			}
			flags[dtMp[i].idWil] = true;

			if(dtMp[i].idWil > 100) {
				fill = 'red';
				label = dtMp[i].namaWil;
				scale = 5;
				if(dtMp[i].namaWil.search("Kota")<0) {
					fill = 'blue';
					label = 'Kabupaten '+dtMp[i].namaWil;    	
					scale = 5;
				}
			} else {
				fill = 'orange';
				label = 'Provinsi '+dtMp[i].namaWil;
				scale = 8;
			}
			locations.push({
				idWil:dtMp[i].idWil,
				namaWil:dtMp[i].namaWil,
				//noUrut:dtMp[i].noUrut,
				//jalur:jalur,
				lat:dtMp[i].lat,
				lng:dtMp[i].lng,
				scale:scale,
				fill:fill,
				label:label,
				//calon:dtMp[i].Kd,
				//wcalon:dtMp[i].Wkd,
				calons:[{noUrut:dtMp[i].noUrut,Kd:dtMp[i].Kd,Wkd:dtMp[i].Wkd,jalur:jalur}]
			});
		}
	}

	// option
	var options = {
		zoom: defaultZoom,
		center: defaultCenter,
		minZoom: 3,
		maxZoom: 10,
		fullscreenControl: false,
		mapTypeControl: false,
		streetViewControl: false,
		panControl: false,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'),options);
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');

	// sorting calon dulu euy
	for(var y=0; y<locations.length; y++) {
		sortJSON(locations[y].calons, 'noUrut');
		//console.log(locations[y].calons);
	}
	
	// marker
	var infoWin = new google.maps.InfoWindow({
		maxWidth: 250
	});
	var markers = locations.map(function(location, i) {
		var marker = new google.maps.Marker({
			position: location,
			map: map,
			visible: true,
			//label: { text: location.label, fontSize: "10px"},
			title: "Klik untuk lihat nama calon di "+ location.label,
			icon: {path: google.maps.SymbolPath.CIRCLE, scale: location.scale, fillOpacity: 0.8, fillColor: location.fill, strokeColor: location.fill, strokeWeight: 1}
		});
		google.maps.event.addListener(marker, 'click', function(evt) {
			var isi = "<h3 class='maps__info__title'>"+location.label+"</h3>";
			//var calonsplit = location.calon.split(";");
			//var wcalonsplit = location.wcalon.split(";");
			//var jalursplit = location.jalur.split(";");
			//var noUrutsplit = location.noUrut.split(";");
			//console.log(location.calons);
			for(var y=0; y<location.calons.length; y++) {
				//isi += "<p><strong>Pasangan :</strong><br>";
				isi += "<p class='maps__info__content'>Pasangan nomor urut "+location.calons[y].noUrut+":<br>";
				isi += "<strong>"+location.calons[y].Kd+" - "+location.calons[y].Wkd+"</strong><br>";
				isi += "</p>";
			}
			isi += '<a class="maps__start" href="/kenali-paslon-detail/?idWil='+location.idWil+'">Detail</a>';
			infoWin.setContent(isi);
			infoWin.open(map, marker);
		})
		return marker;
	});  

	//var markerCluster = new MarkerClusterer(map, markers,
	//	{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

	//console.log(JSON.stringify(locations));
	
	// sorting
	function sortJSON(data, key) {
		return data.sort(function (a, b) {
			var x = a[key];
			var y = b[key];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		});
	}

	function toggle_peta(cat) {
		for(var x=0;x<markers.length;x++) {
			//console.log(markers[x].title);
			//console.log(x);
			if(markers[x].title.search(cat)>0) {
				if(!markers[x].getVisible()) {
					markers[x].setVisible(true);
				} else {
					markers[x].setVisible(false);
				}
			}
		}
	}
	//toggle_peta('Kota');
	//toggle_peta('Kabupaten');
	
	// button reset
	function reset_peta(controlDiv, map) {

		// Set CSS for the control border.
		var boksAtas = document.createElement('div');
		boksAtas.style.backgroundColor = '#fff';
		boksAtas.style.border = '2px solid #fff';
		boksAtas.style.borderRadius = '3px';
		boksAtas.style.boxShadow = 'rgba(0, 0, 0, 0.3) 0px 1px 4px -1px';
		boksAtas.style.cursor = 'pointer';
		boksAtas.style.marginTop = '5px';
		boksAtas.style.marginLeft = '5px';
		boksAtas.style.textAlign = 'center';
		boksAtas.title = 'Klik untuk kembali ke tampilan awal';
		controlDiv.appendChild(boksAtas);

		// Set CSS for the control interior.
		var resetPeta = document.createElement('div');
		resetPeta.style.color = 'rgb(0, 0, 0)';
		resetPeta.style.fontFamily = 'Roboto,Arial,sans-serif';
		resetPeta.style.fontSize = '11px';
		resetPeta.style.fontWeight = '500';
		resetPeta.style.lineHeight = '1.3';
		resetPeta.style.paddingTop = '5px';
		resetPeta.style.paddingBottom = '5px';
		resetPeta.style.paddingLeft = '10px';
		resetPeta.style.paddingRight = '10px';
		resetPeta.innerHTML = 'Reset Peta';
		boksAtas.appendChild(resetPeta);

		// Setup the click event listeners: simply set the map to Chicago.
		boksAtas.addEventListener('click', function() {
			map.setCenter(defaultCenter);
			map.setZoom(defaultZoom)
		});
	}
	var reset_petaDiv = document.createElement('div');
	var reset_peta = new reset_peta(reset_petaDiv, map);

	reset_petaDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(reset_petaDiv);


	//tampilkan legenda peta
	function legendapeta(controlDiv, map) {

		// Set CSS for the control border.
		var legenda = document.createElement('div');
		legenda.style.backgroundColor = '#fff';
		legenda.style.border = '2px solid #fff';
		legenda.style.borderRadius = '3px';
		legenda.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
		//legenda.style.cursor = 'pointer';
		legenda.style.marginTop = '5px';
		legenda.style.marginLeft = '5px';
		legenda.style.textAlign = 'left';
		//legenda.title = 'Keterangan peta';
		controlDiv.appendChild(legenda);

		// Set CSS for the control interior.
		var isiLegenda = document.createElement('div');
		isiLegenda.className = "maps__legend";
		isiLegenda.style.color = 'rgb(25,25,25)';
		isiLegenda.style.fontFamily = 'Roboto,Arial,sans-serif';
		isiLegenda.style.fontSize = '14px';
		//isiLegenda.innerHTML = '<h3 class="maps__legend__title">Keterangan:</h3>';
		legenda.appendChild(isiLegenda);
		
		var wbtnProv = document.createElement('label');
		wbtnProv.className = "maps__label";
		isiLegenda.appendChild(wbtnProv);
		var btnProv = document.createElement('input');
		btnProv.type = 'checkbox';
		btnProv.checked = 'checked';
		btnProv.id = 'buttonProv';
		btnProv.value = 'Provinsi';
		wbtnProv.appendChild(btnProv);
		var txtProv = document.createTextNode('Pilkada Provinsi');
		wbtnProv.appendChild(txtProv);
		var iProv = document.createElement('span');
		iProv.className = 'maps__icon maps__icon--prov';
		wbtnProv.appendChild(iProv);
		btnProv.addEventListener('click', function () {
			toggle_peta('Prov');
		});

		var wbtnKab = document.createElement('label');
		wbtnKab.className = "maps__label";
		isiLegenda.appendChild(wbtnKab);
		var btnKab = document.createElement('input');
		btnKab.type = 'checkbox';
		btnKab.checked = 'checked';
		btnKab.id = 'buttonKab';
		btnKab.value = 'Kabupaten';
		wbtnKab.appendChild(btnKab);
		var txtKab = document.createTextNode('Pilkada Kabupaten');
		wbtnKab.appendChild(txtKab);
		var iKab = document.createElement('span');
		iKab.className = 'maps__icon maps__icon--kab';
		wbtnKab.appendChild(iKab);
		btnKab.addEventListener('click', function () {
			toggle_peta('Kab');
		});

		var wbtnKota = document.createElement('label');
		wbtnKota.className = "maps__label";
		isiLegenda.appendChild(wbtnKota);
		var btnKota = document.createElement('input');
		btnKota.type = 'checkbox';
		btnKota.checked = 'checked';
		btnKota.id = 'buttonKota';
		btnKota.value = 'Kota';
		wbtnKota.appendChild(btnKota);
		var txtKota = document.createTextNode('Pilkada Kota');
		wbtnKota.appendChild(txtKota);
		var iKota = document.createElement('span');
		iKota.className = 'maps__icon maps__icon--kota';
		wbtnKota.appendChild(iKota);
		btnKota.addEventListener('click', function () {
			toggle_peta('Kota');
		});

	}

	var legendapetaDiv = document.createElement('div');
	var legendapeta = new legendapeta(legendapetaDiv, map);

	legendapetaDiv.index = 1;
	map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legendapetaDiv);

}
