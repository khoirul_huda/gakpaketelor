$(document).ready(function() {

	var url = location.href;
	if (url.indexOf("?") >= 0) {
		var url2 = url.split("?")[1];
		if (url2.length>0) {
			var idWilUrl = url2.split("=")[1];
			// cek url dulu cuy
			if(idWilUrl.length>0) {
				
				// by pass 
				if(idWilUrl==12 || idWilUrl==32) {
					
					// tampilin calon,
					var mulai = 0;
					var tampungCalon = [];
					var dataC = dfc.aaDataCalon;
					for (var i=0; i < dataC.length; i++) {
						// load pertanyaan daerah yg dipilih, kumpulin jadi satu vars
						if(dataC[i].idWil == idWilUrl) {
							// tampung 
							tampungCalon.push({
								idWil:dataC[i].idWil,
								cnamaWil:dataC[i].namaWil,
								cKd:dataC[i].Kd,
								cWkd:dataC[i].Wkd,
								cnoUrut:dataC[i].noUrut
							});
						}
					}
					// lagi
					var templatepreambule = $('#quiz').append('<div class="quiz__opening active"><h3>'+tampungCalon[0].cnamaWil+'</h3><p>Kamu yakin tidak akan terjebak isu HOAKS <br>tentang pasangan calon Kepala Daerah di bawah ini?</p></div>');
					for (var k=0; k < tampungCalon.length; k++) {
						$('.quiz__opening').append('<div class="quiz__opening__item"><p>No urut '+tampungCalon[k].cnoUrut+': <br> <strong>'+tampungCalon[k].cKd+' <br> '+tampungCalon[k].cWkd+'</strong></p></div>');
					}
					$('.quiz__opening').append('<div class="quiz__word">Klik tombol "Mulai" untuk memainkan kuis</div><div class="quiz__lanjut active"><a href="#" class="quiz__lnjt" id="quiz__start" data-start="1">Mulai</a></div>');

					// mulai
					$('.quiz__opening').on('click','#quiz__start', function(e) {
						// cek udah habis belom pertanyaannya
						e.preventDefault();
						$('.quiz__opening').removeClass('active');
						$("html, body").animate({scrollTop: 0}, 500);
					});
					

					// load pertanyaan
					var tandain = [];
					var tampungQuestion = [];
					var dataQ = dfq.questionData;
					for (var i=0; i < dataQ.length; i++) {
						// load pertanyaan daerah yg dipilih, kumpulin jadi satu vars
						if(dataQ[i].idWil == idWilUrl) {
							var qDaerah = dataQ[i].namaWil;
							if(dataQ[i].hoaks=="hoaks") {
								var qstatus = 1;
							} else {
								var qstatus = 2;
							}
							// tampung 
							tampungQuestion.push({
								idWil:dataQ[i].idWil,
								qQuestion:dataQ[i].question,
								qImg:dataQ[i].img,
								qLink:dataQ[i].link,
								qLinktitle:dataQ[i].linktitle,
								qAns:qstatus,
								qHoaks:dataQ[i].hoaks
							});
						}
					}

					// klo udah tampilin tuh 
					var tampil = 0;
					var lanjut = 0;
					while(tampil < tampungQuestion.length) {
						var maks = tampungQuestion.length;
						var templatequiz = $('#quiz').append('<div class="quiz__item'+(tampil==0?" active":"")+'" id="quiz'+(tampil+1)+'" data-id="'+(tampil+1)+'" data-answer="'+tampungQuestion[tampil].qAns+'"></div>');
						var templatequiz2 = $('#quiz'+(tampil+1)).append('<div class="quiz__header">Isu '+(tampil+1)+'</div><div class="quiz__question">'+tampungQuestion[tampil].qQuestion+'</div>')
							.append((tampungQuestion[tampil].qImg=='-'?'':'<div class="quiz__img"><img src="http://www.gakpaketelor.com/asset/gen_images/'+tampungQuestion[tampil].qImg+'"></div>'))
							.append('<div class="quiz__pilihan"><a href="#" class="button quiz__button quiz__button--hoax" data-type="btn-quiz" data-send="1">Hoaks</a><a href="#" class="button quiz__button quiz__button--fact" data-type="btn-quiz" data-send="2">Fakta</a></div>')
							.append('<div class="quiz__penjelasan"><span class="qw"></span> Isu di atas adalah <span class="hoaks">'+tampungQuestion[tampil].qHoaks+'</span></strong> <br>Cek fakta kebenarannya di berita ini: <a href="'+tampungQuestion[tampil].qLink+'" target="_blank" class="titlelink">'+tampungQuestion[tampil].qLinktitle+'</a></div>')
							.append('<div class="quiz__lanjut">'+(tampil==(maks-1)?'<a href="#" class="quiz__result" id="quiz__result" data-aid="'+(tampil+1)+'">Lihat hasil</a>':'<a href="#" class="quiz__lnjt" id="quiz__lanjut" data-aid="'+(tampil+1)+'">Lanjut</a>')+'</div>');
						tampil++;
					}


					// cek jawaban 
					var total = 0;
					$('.quiz__item').on('click','.quiz__button', function(e) {
						e.preventDefault();
						// nyari parent
						var iii = $(this).parent().parent();
						// kunci 
						var key = iii.attr('data-answer');
						// yg dia piliha
						var ans = $(this).attr('data-send');
						// bandingin
						if (key == ans) {
							// bener 
							iii.find('.quiz__penjelasan .qw').html('Kamu <span class="status benar">benar!</span> ');
							iii.find('.quiz__pilihan').hide();
							iii.find('.quiz__penjelasan').addClass('active');
							iii.find('.quiz__lanjut').addClass('active');
							// masukin total
							total++;
						} else {
							iii.find('.quiz__penjelasan .qw').html('Kamu <span class="status salah">salah!</span> ');
							iii.find('.quiz__pilihan').hide();
							iii.find('.quiz__penjelasan').addClass('active');
							iii.find('.quiz__lanjut').addClass('active');
						}
					});

					// lanjutttt
					$('.quiz__item').on('click','.quiz__lnjt', function(e) {
						// cek udah habis belom pertanyaannya
						e.preventDefault();
						var tar = $(this).data('aid');
						if(tar == maks) {
							// final
						} else {
							// mundur
							var ppp = $(this).parent().parent().removeClass('active');
							$('#quiz'+(tar+1)).addClass('active');
						}
						$("html, body").animate({scrollTop: 0}, 500);
					});

					// result 
					$('.quiz__item').on('click','.quiz__result', function(e) {
						e.preventDefault();
						$("html, body").animate({scrollTop: 0}, 500);
						var rrr = $(this).parent().parent();
						console.log(total);
						if(total>2) {
							var yey = '<dis class="quiz__result__yey">Ayo Terus Semangat Memberantas HOAKS!</div>';
						} else {
							var yey = '<dis class="quiz__result__yey">Jangan Mudah Terjebak Berita HOAKS!</div>';
						}
						var res = $('#quiz').html('<div class="quiz__result__box">'+
												  '<div class="quiz__result__yey">'+(total>2?"Ayo Terus Semangat Memberantas HOAKS!":"Jangan Mudah Terjebak Berita HOAKS!")+'</div>'+
												  '<p>Kamu telah menjawab <span>'+total+'</span> dari <span>'+maks+'</span> isu dengan benar. </p>'+
												  '<p>Terima kasih telah telah berpartisipasi dalam upaya memberantas hoaks!</p>'+
												  '<p>Apakah ada berita, foto, atau video yang Kamu ragukan kebenarannya? <a href="https://inside.kompas.com/laporkan-hoaks" class="quiz__button__laporkan" target="_blank">Laporkan!</a></p>'+
												  '<p>Klik <a href="https://indeks.kompas.com/topik-pilihan/list/4390/berita.viral.hoaks.atau.fakta" target="_blank">di sini</a> untuk mengecek fakta lainnya.</p>'+
												  '<p>Klik <a href="/kenali-paslon-detail/?idWil='+idWilUrl+'">di sini</a> untuk melihat lebih lengkap tentang pasangan calon di daerahmu ini.</p>'+
												  '<a class="quiz__lagi" href="/gen-project/">Main lagi</a>'+
												  '<a class="quiz__result__share" href="http://www.facebook.com/sharer/sharer.php?u=http://www.gakpaketelor.com/gen-project/" target="_blank">Share</a>'+
												  '</div>');
					});

					console.log(tampungQuestion);
					console.log(maks);
					
				// end bypass
				} else {
					$('#quiz').html('<p>Quiz belum tersedia, silakan pilih daerah lainnya. <a href="/gen-project">Back to home</a></p>')
				}

			}

		}
	} else {
		$('#quiz').html('<p>Quiz belum tersedia, silakan pilih daerah lainnya. <a href="/gen-project">Back to home</a></p>')
	}

	
});